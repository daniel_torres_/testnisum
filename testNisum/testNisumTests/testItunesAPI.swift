//
//  testNisumTests.swift
//  testNisumTests
//
//  Created by Daniel Torres on 7/9/18.
//  Copyright © 2018 dansTeam. All rights reserved.
//

import XCTest
@testable import testNisum

class testItunesAPI: XCTestCase {
    
    var api: MusicSearcher?
    
    func testSearchTerm() {
        api = ItunesAPI()
        
        let expectation1 = expectation(description: "testSearchTerm")
        
        api!.searchForMedia(term: "in utero", limit: 20) { (tracks) in
            guard !tracks.isEmpty else{
                XCTFail()
                expectation1.fulfill()
                return
            }
            print(tracks)
            XCTAssert(true)
            expectation1.fulfill()
        }
        
        wait(for: [expectation1], timeout: 20)
    }
    
    func testAlbumTracksTerm() {
        api = ItunesAPI()
        
        let expectation1 = expectation(description: "testAlbumTracksTerm")
        
        api!.searchTracksAndAlbumFrom(albumID: "879273552") { (tracksAndALbumTuple) in
            
            let tracks = tracksAndALbumTuple.0
            
            guard !tracks.isEmpty else{
                XCTFail()
                expectation1.fulfill()
                return
            }
            XCTAssert(true)
            expectation1.fulfill()
        }
        
        wait(for: [expectation1], timeout: 20)
    }
    
    
}
