//
//  DetailMediaItemViewController.swift
//  testNisum
//
//  Created by Daniel Torres on 7/9/18.
//  Copyright © 2018 dansTeam. All rights reserved.
//

import UIKit

class DetailMediaItemViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var delegate : MediaItemSearchTableViewDelegate? = TrackListTableViewDelegate(mediaItems: [MediaItem]())
    var dataSource: MediaItemSearchTableViewDataSource? = TracksListDataSource(mediaItems: [MediaItem]())
    
    var mediaItemSelected: Track!
    var api: MusicSearcher!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureTableView()
        title = "Detail selected"
     
        getTracks(withIDAlbum: String(mediaItemSelected.album.id))
    }
    
    func configureTableView() {
        
        let delegateMetadaView = MetaDataItemPresenter(item: mediaItemSelected.album)
        let metadataview : MetaDataDetailItem = MetaDataDetailItem().loadViewFromNib() as! MetaDataDetailItem
        metadataview.delegate = delegateMetadaView
        metadataview.setUp()
        
        metadataview.translatesAutoresizingMaskIntoConstraints = false
        tableView.tableHeaderView?.addSubview(metadataview)
        metadataview.centerXAnchor.constraint(equalTo: tableView.tableHeaderView!.centerXAnchor).isActive = true
        metadataview.centerYAnchor.constraint(equalTo: tableView.tableHeaderView!.centerYAnchor).isActive = true
        metadataview.widthAnchor.constraint(equalTo: tableView.tableHeaderView!.widthAnchor).isActive = true
        metadataview.heightAnchor.constraint(equalTo: tableView.tableHeaderView!.heightAnchor).isActive = true
        tableView.tableHeaderView?.heightAnchor.constraint(equalToConstant: 200)
        
        tableView.dataSource = dataSource
        tableView.delegate = delegate
    }
    
    func configureDataSources(withItems items: [MediaItem]) {
        dataSource?.mediaItems = items
        delegate?.mediaItems = items
    }
    
    func getTracks(withIDAlbum: String){
        api.searchTracksAndAlbumFrom(albumID: withIDAlbum, finishedBlock: { [weak self] (tracksAlbumTupple) in
            guard let `self` = self else {
                return
            }
            let tracks = tracksAlbumTupple.0
            
            DispatchQueue.main.async {
                `self`.configureDataSources(withItems: tracks)
                `self`.tableView.reloadData()
            }
        })
    }

}
