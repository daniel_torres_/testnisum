//
//  ViewController.swift
//  testNisum
//
//  Created by Daniel Torres on 7/9/18.
//  Copyright © 2018 dansTeam. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var messageLabel: UILabel!
    
    //can be another updater outside the viewcontroller. this can be made with the coordinator patter.
    //but because this is a simple app lets keep updater with this default value.
    var updater : UpdaterResults = UpdaterResults(api: ItunesAPI())
    
    //same here delegate and data source can be another as long as it conforms
    //to MediaItemSearchTableViewDelegate and MediaItemSearchTableViewDataSource respectevly
    typealias PagableAndSearchable = MediaItemSearchTableViewDelegate & Pagable
    
    var delegate: PagableAndSearchable? = MediaItemListTableViewDelegate(mediaItems: [MediaItem]())
    var dataSource: MediaItemSearchTableViewDataSource? = MediaItemsListTableViewDataSource(mediaItems: [MediaItem]())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
        
        title = "Search"
        
        searchBar.delegate = updater
        updater.delegate = self
        
        delegate?.delegatePager = updater
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    
    func configureTableView() {
        tableView.dataSource = dataSource
        tableView.delegate = delegate
    }
    
    func configureDataSources(withItems items: [MediaItem]) {
        dataSource?.mediaItems = items
        delegate?.mediaItems = items
    }

}

extension ViewController: UpdaterResultsDelegate{
    func didPressButtonSearch() {
        searchBar.resignFirstResponder()
    }
    
    func didReceivedNew(items: [Searchable], forQuery query: String?) {
        guard let mediaItems = items as? [MediaItem] else{
            return
        }
        
        guard !mediaItems.isEmpty else {
            tableView.isHidden = true
            if let query = query, query != ""{
                messageLabel.isHidden = false
                messageLabel.text = "Search term \"\(query)\" not found"
            }else{
                messageLabel.isHidden = true
            }
            return
        }
         
        messageLabel.isHidden = true
        tableView.isHidden = false
        configureDataSources(withItems: mediaItems)
        
        //reloads tableview when new items arrived from updaterResults object.
        tableView.reloadData()
    }
    
    
}

