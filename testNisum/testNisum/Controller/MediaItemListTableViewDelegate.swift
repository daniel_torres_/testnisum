//
//  MediaItemListTableViewDelegate.swift
//  testNisum
//
//  Created by Daniel Torres on 7/9/18.
//  Copyright © 2018 dansTeam. All rights reserved.
//

import UIKit


protocol MediaItemSearchTableViewDelegate: UITableViewDelegate {
    var mediaItems: [MediaItem] { get set}
    init(mediaItems: [MediaItem])
}

protocol Pagable: class {
    var delegatePager: PagerDelegate? { get set }
}

protocol PagerDelegate: class {
    func page()
}

class MediaItemListTableViewDelegate: NSObject, MediaItemSearchTableViewDelegate, Pagable{
    var mediaItems: [MediaItem]
    
    weak var delegatePager: PagerDelegate? = nil
    
    required init(mediaItems: [MediaItem]){
        self.mediaItems = mediaItems
        super.init()
    }
    
    func setDelegatePager(_ delegatePager: PagerDelegate){
        self.delegatePager = delegatePager
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)) {
            delegatePager?.page()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        guard let currentVC = UIViewController.currentViewController() else {
            tableView.deselectRow(at: indexPath, animated: true)
            return
        }
        
        if let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DetailMediaItemViewController") as? DetailMediaItemViewController {
            
            if let mediaItemSelected = mediaItems[indexPath.row] as? Track {
                vc.mediaItemSelected = mediaItemSelected
                vc.api = ItunesAPI()
                currentVC.navigationController?.pushViewController(vc, animated: true)
            }
        }
        
        
    }
    
}


class TrackListTableViewDelegate: NSObject, MediaItemSearchTableViewDelegate {
    var mediaItems: [MediaItem]
    var playerPreview: PlayerPreview?
    
    required init(mediaItems: [MediaItem]){
        self.mediaItems = mediaItems
        playerPreview = nil
        super.init()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let reproduableTrackURL = mediaItems[indexPath.row] as? Reproduceable else{
            return
        }
        
        playerPreview = PlayerPreview(withItem: reproduableTrackURL)
        playerPreview?.play()
    }
    
}
