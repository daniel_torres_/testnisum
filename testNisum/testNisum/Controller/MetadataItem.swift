//
//  MetaDataDetailItemDelegate.swift
//  testNisum
//
//  Created by Daniel Torres on 7/9/18.
//  Copyright © 2018 dansTeam. All rights reserved.
//

import Foundation

class MetaDataItemPresenter: NSObject, MetaDataDetailItemDelegate{
    var item: MetaDataItemFormat
    
    let imageProvider: ImageProvider = ImageProvider()
    
    init(item: MetaDataItemFormat) {
        self.item = item
        super.init()
    }
    
    func setUp(_ view: MetaDataDetailItem) {
        view.bandName?.text = item.subTitleMetaData
        view.albumName?.text = item.titleMetaData
        
        
        guard let item = item as? MediaItem else {
            return
        }
        
        imageProvider.getImage(fromItem: item as MediaItem) { [weak view] (image) in
            DispatchQueue.main.async {
                view?.albumcover.image = image
            }
        }
    }
}

