//
//  MediaItemsListDataSource.swift
//  testNisum
//
//  Created by Daniel Torres on 7/9/18.
//  Copyright © 2018 dansTeam. All rights reserved.
//

import UIKit

protocol MediaItemSearchTableViewDataSource: UITableViewDataSource {
    var mediaItems: [MediaItem] { get set}
    init(mediaItems: [MediaItem])
}

class MediaItemsListTableViewDataSource: NSObject, MediaItemSearchTableViewDataSource{
    
    var mediaItems: [MediaItem]
    
    required init(mediaItems: [MediaItem]){
        self.mediaItems = mediaItems
        super.init()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mediaItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "MediaItemCell", for: indexPath) as? MediaItemTableViewCell else{
            return UITableViewCell()
        }
        
        guard let presentableTrack = mediaItems[indexPath.row] as? MediaItemCellPresentable
             else{
            return UITableViewCell()
        }
        
        cell.textLabel?.text = presentableTrack.title
        cell.detailTextLabel?.text = presentableTrack.subtitle
        
        return cell
    }
}

class TracksListDataSource: MediaItemsListTableViewDataSource {
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell = super.tableView(tableView, cellForRowAt: indexPath)
        cell.detailTextLabel?.isHidden = true
        return cell
    }
}
