//
//  MetaDataDetailItem.swift
//  testNisum
//
//  Created by Daniel Torres on 7/9/18.
//  Copyright © 2018 dansTeam. All rights reserved.
//

import UIKit

protocol MetaDataDetailItemDelegate: class{
    var item: MetaDataItemFormat {get set}
    func setUp(_: MetaDataDetailItem)
}

protocol MetaDataItemFormat {
    var titleMetaData: String { get }
    var subTitleMetaData: String { get }
}

class MetaDataDetailItem: UIView, LoadableNib{
    
    @IBOutlet var albumName: UILabel!
    @IBOutlet var bandName: UILabel!
    @IBOutlet var albumcover: UIImageView!
    
    weak var delegate: MetaDataDetailItemDelegate?
    
    var nibName: String? {
        return "MetaDataDetailItem"
    }
    
    func setUp(){
        delegate?.setUp(self)
    }
    
}
