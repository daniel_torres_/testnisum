//
//  CellPresentable.swift
//  testNisum
//
//  Created by Daniel Torres on 7/9/18.
//  Copyright © 2018 dansTeam. All rights reserved.
//

import Foundation


protocol MediaItemCellPresentable {
    var title: String { get }
    var subtitle: String { get }
}
