//
//  ImageProvider.swift
//  testNisum
//
//  Created by Daniel Torres on 7/9/18.
//  Copyright © 2018 dansTeam. All rights reserved.
//

import UIKit


class ImageProvider: ImageDownloader {
    
    func getImage(fromItem item: MediaItem,
                  finishedBlock: @escaping ((UIImage?) -> ())) {
    
        
        guard let item = item as? Ilustrable else{
            return finishedBlock(nil)
        }
        
        DispatchQueue.global(qos: .userInteractive).async {
            guard let urllink = URL(string: item.imageURL) else{
                    DispatchQueue.main.async {
                        finishedBlock(nil)
                    }
                    return
            }
            
            do {
                let image = try self.getImageFrom(urllink: urllink)
                DispatchQueue.main.async {
                    finishedBlock(image)
                }
            }catch let e{
                print(e)
                DispatchQueue.main.async {
                    finishedBlock(nil)
                }
            }
        }
    }
}
