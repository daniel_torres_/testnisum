//
//  Player.swift
//  testNisum
//
//  Created by Daniel Torres on 7/10/18.
//  Copyright © 2018 dansTeam. All rights reserved.
//

import AVFoundation

protocol Reproduceable{
    var urlStreaming: String { get }
}

class PlayerPreview: NSObject{
    
    private var player: AVPlayer

    init?(withItem: Reproduceable){
        
        guard let url = URL(string: withItem.urlStreaming)
            else {
                return nil
        }
        let playerItem = AVPlayerItem.init(url: url)
        player = AVPlayer.init(playerItem: playerItem)
        super.init()
    }
    
    func play(){
        player.play()
    }
    
    
    
}


