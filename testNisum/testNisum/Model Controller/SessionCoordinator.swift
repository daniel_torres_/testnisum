//
//  SessionCoordinator.swift
//  testNisum
//
//  Created by Daniel Torres on 7/9/18.
//  Copyright © 2018 dansTeam. All rights reserved.
//

import Foundation

struct SessionsCoordinator {
    static let cacheSession = URLSession.shared.cacheSession
    static let defaultSession = URLSession.shared.normalSession
}

extension URLSession {
    var cacheSession : URLSession {
        let defaultConfiguration = URLSessionConfiguration.default
        
        // Configuring caching behavior for the default session
        let cachesDirectoryURL = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first!
        let cacheURL = cachesDirectoryURL.appendingPathComponent("Cache Session")
        let diskPath = cacheURL.path
        
        let cache = URLCache(memoryCapacity: 10 * 1024 * 1024, diskCapacity: 120 * 1024 * 1024, diskPath: diskPath)
        defaultConfiguration.urlCache = cache
        defaultConfiguration.requestCachePolicy = .returnCacheDataElseLoad
        defaultConfiguration.httpShouldUsePipelining = false
        
        return URLSession(configuration: defaultConfiguration)
    }
    
    var normalSession : URLSession {
        let defaultConfiguration = URLSession.shared.configuration
        
        defaultConfiguration.httpShouldUsePipelining = true
        defaultConfiguration.waitsForConnectivity = true
        
        return URLSession(configuration: defaultConfiguration)
    }
}
