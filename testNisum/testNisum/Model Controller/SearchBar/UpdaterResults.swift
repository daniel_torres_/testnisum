//
//  UpdaterResults.swift
//  testNisum
//
//  Created by Daniel Torres on 7/9/18.
//  Copyright © 2018 dansTeam. All rights reserved.
//

import UIKit

protocol UpdaterResultsDelegate: class{
    func didReceivedNew(items: [Searchable], forQuery: String?)
    func didPressButtonSearch()
}

protocol UpdaterResultsAPI {
    func search(term: String, pager: Pager, finishedBlock: @escaping ([Searchable]?) -> Void)
}

class UpdaterResults : NSObject, UISearchBarDelegate, PagerDelegate{
    
    
    //an searcher api, takes a term argument and returns a list of decodable objects.
    private let api: UpdaterResultsAPI

    //takes care the results of the api consumed.
    weak var delegate: UpdaterResultsDelegate?
    private var pager: Pager = Pager()
    
    init(api: UpdaterResultsAPI) {
        self.api = api
        super.init()
    }
    
    private var searching: Bool = false
    
    //controls the request with a timeinterval defined
    private var throtle = Timer()
    private var queryToSearchString = ""
    private var searchedQueryString = ""
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        guard let query = searchBar.text else {
            return
        }
        
        pager.reset()
        queryToSearchString = query
        
        guard !throtle.isValid else {
            return
        }
        
        throtle = Timer.scheduledTimer(timeInterval: 0.3,
                                       target: self,
                                       selector: #selector(resetTimer),
                                       userInfo: nil,
                                       repeats: false)
    }
    
    
    @objc
    private func resetTimer(){
        
        throtle.invalidate()
        guard searchedQueryString != queryToSearchString else {
            return
        }
        searchedQueryString = queryToSearchString
        search(query: queryToSearchString)
    }
    
    private func search(query: String) {
        searching = true
        api.search(term: query, pager: pager) { [weak self] (items) -> Void in
            guard let `self` = self else{
                return
            }
            
            guard let items = items else {
                //send empty
                DispatchQueue.main.async {
                    `self`.delegate?.didReceivedNew(items: [Searchable](), forQuery: query)
                    `self`.searching = false
                }
                return
            }
            
            //excute update UI in the main thread.
            DispatchQueue.main.async {
                `self`.delegate?.didReceivedNew(items: items, forQuery: query)
                `self`.searching = false
            }
        }
    }
    
    func page() {
        if  !searching && !pager.limitReached
        {
            pager.doPage()
            search(query: searchedQueryString)
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        delegate?.didPressButtonSearch()
    }
}
