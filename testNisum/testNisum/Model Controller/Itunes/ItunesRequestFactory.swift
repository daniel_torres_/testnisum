//
//  ItunesRequestMaker.swift
//  testNisum
//
//  Created by Daniel Torres on 7/9/18.
//  Copyright © 2018 dansTeam. All rights reserved.
//

import Foundation

protocol RequestMaker {
    
    var host: String { get }
    var scheme: String { get }
    var httpMethod: String { get }
    
    func makeRequest() -> URLRequest
}

extension RequestMaker{
    var host: String {
        return Constants.Itunes.host
    }
    var scheme: String {
        return Constants.Itunes.scheme
    }
    var httpMethod: String {
        return Constants.HttpMethod.GET.rawValue
    }
}

class ItunesAlbumRequestMaker : RequestMaker {
    
    let id: String
    
    init(id: String) {
        self.id = id
    }
    
    func makeRequest() -> URLRequest {
        var urlComponents = URLComponents()
        urlComponents.scheme = scheme
        urlComponents.host = host
        urlComponents.path = Constants.Itunes.Path.lookup
        
        let idQueryItem = URLQueryItem(name: "id", value: id)
        let entityQueryItem = URLQueryItem(name: "entity", value: "song")
        urlComponents.queryItems = [URLQueryItem]()
        urlComponents.queryItems!.append(idQueryItem)
        urlComponents.queryItems!.append(entityQueryItem)
        
        assert(urlComponents.url != nil)
        // Create and configure the `URLRequest`.
        var urlRequest = URLRequest(url: urlComponents.url!)
        urlRequest.httpMethod = httpMethod

        return urlRequest
    }
}

class ItunesSearchTermRequestMaker : RequestMaker {
    
    let term: String
    let limit: Int
    
    init(term: String, limit: Int) {
        self.term = term
        self.limit = limit
    }
    
    func makeRequest() -> URLRequest {
        var urlComponents = URLComponents()
        urlComponents.scheme = scheme
        urlComponents.host = host
        
        urlComponents.path = Constants.Itunes.Path.search
        let stringTerm = term.replacingOccurrences(of: " ", with: "+")
        
        let termQueryItem = URLQueryItem(name: "term", value: stringTerm)
        let limitQueryItem = URLQueryItem(name: "limit", value: String(limit))
        let musicQueryItem = URLQueryItem(name: "mediaType", value: "music")
        let entityQueryItem = URLQueryItem(name: "entity", value: "song")
        urlComponents.queryItems = [URLQueryItem]()
        urlComponents.queryItems!.append(termQueryItem)
        urlComponents.queryItems!.append(limitQueryItem)
        urlComponents.queryItems!.append(musicQueryItem)
        urlComponents.queryItems!.append(entityQueryItem)
        
        assert(urlComponents.url != nil)
        // Create and configure the `URLRequest`.
        var urlRequest = URLRequest(url: urlComponents.url!)
        urlRequest.httpMethod = httpMethod
        
        
        
        return urlRequest
    }
}


enum ItunesRequestFactoryOptions {
    case searchTermRequest(term: String, limit: Int)
    case searcTrackAlbums(withIdAlbum: String)
}

enum ItunesRequestFactory {
    static func request(fromOption option: ItunesRequestFactoryOptions) -> URLRequest {
        switch option {
        case .searchTermRequest(let term, let limit) :
            return ItunesSearchTermRequestMaker(term: term, limit: limit).makeRequest()
        case .searcTrackAlbums(let idAlbum):
            return ItunesAlbumRequestMaker(id: idAlbum).makeRequest()
        }
    }
}
