//
//  ItunesAPI.swift
//  testNisum
//
//  Created by Daniel Torres on 7/9/18.
//  Copyright © 2018 dansTeam. All rights reserved.
//

import Foundation


protocol MusicSearcher {
    func searchForMedia(term: String, limit: Int, finishedBlock: @escaping  ([MediaItem]) -> ())
    func searchTracksAndAlbumFrom(albumID: String, finishedBlock: @escaping  (([Track], Album?)) -> ())
}

class ItunesAPI: MusicSearcher, API{
    func searchTracksAndAlbumFrom(albumID: String, finishedBlock: @escaping (([Track], Album?)) -> ()) {
        executeDataRequestWithCustomSession(session: SessionsCoordinator.defaultSession,
                                            request: ItunesRequestFactory.request(fromOption: ItunesRequestFactoryOptions.searcTrackAlbums(withIdAlbum: albumID))) { [weak self] (data) in
                                                
                                                guard let `self` = self else {
                                                    return
                                                }
                                                finishedBlock(`self`.parseDataTracksAndAlbum(data))
        }
    }
    
    func searchForMedia(term: String, limit: Int, finishedBlock: @escaping ([MediaItem]) -> ()) {
        executeDataRequestWithCustomSession(session: SessionsCoordinator.cacheSession,
                                            request: ItunesRequestFactory.request(fromOption: ItunesRequestFactoryOptions.searchTermRequest(term: term, limit: limit))) { [weak self] (data) in
                                                
                                                guard let `self` = self else {
                                                    return
                                                }
                                                finishedBlock(`self`.parseData(data))
                                                
        }
    }
    
    // should be in an object that parses array of tracks given a data.
    private func parseData(_ data: Data?) -> [Track] {
        
        guard let data = data else{
            return [Track]()
        }
        
        let jsonDecoder = JSONDecoder()
        
        guard let searchTermResponse = try? jsonDecoder.decode(SearchTermsResponse.self, from: data) else{
            return [Track]()
        }
        
        return searchTermResponse.results
    }
    
    private func parseDataTracksAndAlbum(_ data: Data?) -> ([Track], Album?) {
        
        guard let data = data else{
            return ([Track](), nil)
        }
        
        
        do {
            guard let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any],
                let response = SearchAlbumTracksResponse(json: json) else{
                return ([Track](), nil)
            }
            
            return (response.tracks, response.album)
        }catch{
            print("there was a problem with parseDataTracksAndAlbum parsing")
            return ([Track](), nil)
        }
        
    }
    
}

extension ItunesAPI: UpdaterResultsAPI{
    func search(term: String, pager: Pager, finishedBlock: @escaping ([Searchable]?) -> Void) {
        searchForMedia(term: term, limit: pager.currentLimit) { (mediaItems) in
            guard !mediaItems.isEmpty,
                let tracks = mediaItems as? [Track] else{
                finishedBlock(nil)
                return
            }
            finishedBlock(tracks)
        }
    }
}



