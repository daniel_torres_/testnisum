//
//  Parsers.swift
//  testNisum
//
//  Created by Daniel Torres on 7/10/18.
//  Copyright © 2018 dansTeam. All rights reserved.
//

import Foundation


struct SearchTermsResponse: Decodable {
    
    let results: [Track]
    
    enum CodingKeys: String, CodingKey {
        case results
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        results = try values.decode([Track].self, forKey: .results)
    }
    
}

struct SearchAlbumTracksResponse {

    let tracks: [Track]
    let album: Album
    
    init?(json: [String: Any]) {
        guard let results = json["results"] as? [[String: AnyObject]] else {
            return nil
        }
        
        guard let first = results.first,
            let album = Album(dict: first),
            results.count > 1 else {
            return nil
        }
        var restTracks = results
        restTracks.remove(at: 0)
        
        var tracksToReturn = [Track]()
        
        for trackDict in restTracks {
            if let track = Track(dict: trackDict) {
                tracksToReturn.append(track)
            }
        }

        self.tracks = tracksToReturn
        self.album = album
    }
    
}

