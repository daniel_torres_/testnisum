//
//  Ilustrable.swift
//  testNisum
//
//  Created by Daniel Torres on 7/9/18.
//  Copyright © 2018 dansTeam. All rights reserved.
//

import Foundation


protocol Ilustrable {
    var imageURL: String { get }
}
