//
//  Imagedownloader.swift
//  testNisum
//
//  Created by Daniel Torres on 7/9/18.
//  Copyright © 2018 dansTeam. All rights reserved.
//

import UIKit

protocol ImageDownloader{
    func getImageFrom(urllink: URL) throws -> UIImage?
}

extension ImageDownloader{
    func getImageFrom(urllink: URL) throws -> UIImage?{
        do{
            let data = try Data(contentsOf: URL(string: urllink.absoluteString)! )
            guard let uiimage = UIImage(data: data) else {
                return nil
            }
            return uiimage
        }catch{
            return nil
        }
    }
}
