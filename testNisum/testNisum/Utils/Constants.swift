//
//  Constants.swift
//  testNisum
//
//  Created by Daniel Torres on 7/9/18.
//  Copyright © 2018 dansTeam. All rights reserved.
//

import Foundation


struct Constants {
    
    struct Itunes{
        static let host = "itunes.apple.com"
        static let scheme = "https"
        
        struct Path {
            static let search = "/search"
            static let lookup = "/lookup"
        }
    }
    
    
    enum HttpMethod: String {
        case GET
        case POST
    }
    
}
