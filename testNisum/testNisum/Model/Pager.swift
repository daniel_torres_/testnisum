//
//  Pager.swift
//  testNisum
//
//  Created by Daniel Torres on 7/9/18.
//  Copyright © 2018 dansTeam. All rights reserved.
//

import Foundation


struct Pager{
    
    var currentLimit: Int {
        return limit
    }
    
    var limitReached: Bool {
        return limit >= 200
    }
    
    private var limit: Int = 0
    
    mutating func doPage(){
        let newLimit = limit + 20
        if ((newLimit) > 200) {
            limit = 200
        }else{
            limit = newLimit
        }
        
    }
    
    mutating func reset() {
        limit = 0
    }
    
}
