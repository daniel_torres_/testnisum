//
//  Track.swift
//  testNisum
//
//  Created by Daniel Torres on 7/9/18.
//  Copyright © 2018 dansTeam. All rights reserved.
//

import Foundation

protocol Searchable {
    
}

protocol MediaItem {
    var id: Int {get set}
}

struct Track: MediaItem, Searchable {
    var name: String
    var album: Album
    var id: Int
    var previewUrl: String
    
    enum CodingKeys: String, CodingKey {
        case name = "trackName"
        case trackId = "trackId"
        case previewUrl = "previewUrl"
    }
    
    
    init?(dict: [String: AnyObject]){
        
        guard let name = dict[CodingKeys.name.rawValue] as? String,
            let trackId = dict[CodingKeys.trackId.rawValue] as? Int,
            let previewUrl = dict[CodingKeys.previewUrl.rawValue] as? String,
            let album = Album(dict: dict) else{
            return nil
        }
        
        self.name = name
        self.album = album
        self.id = trackId
        self.previewUrl = previewUrl
    }
}


extension Track: Reproduceable {
    var urlStreaming: String {
        return previewUrl
    }
}

extension Track: Ilustrable {
    var imageURL: String{
        return album.urlCover
    }
}

extension Track: Decodable {
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        name = try values.decode(String.self, forKey: .name)
        album = try Album(from: decoder)
        id = try values.decode(Int.self, forKey: .trackId)
        previewUrl = try values.decode(String.self, forKey: .previewUrl)
    }
}


extension Track: MediaItemCellPresentable{
    
    var title: String {
        return "\(name)"
    }
    
    var subtitle: String {
        return "\(album.artist) - \(album.album)"
    }
}
