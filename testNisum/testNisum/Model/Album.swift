//
//  Album.swift
//  testNisum
//
//  Created by Daniel Torres on 7/10/18.
//  Copyright © 2018 dansTeam. All rights reserved.
//

import Foundation

struct Album: MediaItem {
    var album: String
    var artist: String
    var urlCover: String
    var id: Int
    
    enum CodingKeys: String, CodingKey {
        case collectionId = "collectionId"
        case album = "collectionName"
        case artist = "artistName"
        case urlCover = "artworkUrl100"
    }
    
    init?(dict: [String: AnyObject]) {
        
        guard let album = dict[CodingKeys.album.rawValue] as? String,
            let artist = dict[CodingKeys.artist.rawValue] as? String,
            let id = dict[CodingKeys.collectionId.rawValue] as? Int,
            let urlCover = dict[CodingKeys.urlCover.rawValue] as? String else{
                return nil
        }
        
        self.album = album
        self.artist = artist
        self.urlCover = urlCover
        self.id = id
    }
}

extension Album: MetaDataItemFormat{
    var titleMetaData: String {
        return album
    }
    
    var subTitleMetaData: String {
        return artist
    }
}

extension Album: Ilustrable {
    
    var imageURL: String{
        return urlCover
    }
}

extension Album: Decodable {
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)

        album = try values.decode(String.self, forKey: .album)
        artist = try values.decode(String.self, forKey: .artist)
        urlCover = try values.decode(String.self, forKey: .urlCover)
        id = try values.decode(Int.self, forKey: .collectionId)
    }
}


extension Album: MediaItemCellPresentable{
    
    var title: String {
        return ""
    }
    
    var subtitle: String {
        return "\(artist) - \(album)"
    }
}
